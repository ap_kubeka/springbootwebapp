# Use a lightweight base image with JRE to run the application
FROM openjdk:11-jre-slim

ENV version=release_version
# Set the working directory inside the container
WORKDIR /app

# Copy the pre-built JAR file from the CI/CD artifact
COPY target/spring-boot-web-$version-SNAPSHOT.jar /app/spring-boot-web.jar

# Specify the command to run the application
CMD ["java", "-jar", "spring-boot-web.jar"]
