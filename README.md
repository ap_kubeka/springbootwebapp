# DevSecOps with Site Reliability Engineering (SRE)

This project demonstrates how to enhance application security by shifting left using the Fortify's ScanCentral SAST Client for SAST (Static Application Security Testing), Debricked CLI for SCA (Software Composition Analysis), and WebInspect API for DAST (Dynamic Application Security Testing) in a CI/CD pipeline.

## Site Reliability Engineering (SRE) Tools

In addition to enhancing security, this project also incorporates Site Reliability Engineering (SRE) practices using the following tools:

- **Prometheus**: Metrics collection and monitoring system.
- **Grafana**: Data visualization and monitoring tool that works with Prometheus.
- **Nginx**: HTTP load balancer for distributing incoming traffic across multiple servers.
- **Rundeck**: Container management and operations automation platform.

These SRE tools help in improving the reliability, scalability, and performance of the application infrastructure.

## Application Overview

The application used in this project is a Java Spring Boot web application based on the tutorial available at [Spring Framework Guru](https://springframework.guru/spring-boot-web-application-part-1-spring-initializr).

## Build Environment

The build machine for this project is a Windows computer with the following tools installed:

- Java
- Maven
- Docker Desktop
- Gitlab Runner
- Fortify WebInspect
- Fortify ScanCentral SAST Client
- Fortify CLI
- Debricked CLI

## GitLab CI/CD Pipeline Overview

This project utilizes GitLab CI/CD pipeline stages to automate various security testing and deployment processes. Below are the stages used in the `.gitlab-ci.yml` file:

### 1. **Validate Stage**

#### Validate Project

This job validates the Maven project.

### 2. **Scan Stage**

#### Fortify SAST Scan
This job initiates a SAST scan using the ScanCentral Client and uploads the scan results to the SSC (Software Security Center).

![ScanCentral_SAST](images/ScanCentral_SAST.PNG)

#### Debricked SCA Scan
This job performs a software composition analysis using the Debricked CLI, exports the scan results from Debricked and uploads them to the SSC.

![Debricked](images/Debricked.PNG)

### 3. **Prepare Stage**

#### Get Version
This job extracts the version from the generated JAR file, removes the `-SNAPSHOT` suffix, and saves it to a file called `version.txt`. The file is then archived as an artifact for later use.

### 4. **Release Stage**

#### Main Release
This job creates release version tags, pushes the release version to a new tag, and updates the main branch to the next snapshot version.

#### Release Build
This job builds release version tags and retains the artifacts for 2 weeks.

### 5. **Deploy Stage**

#### Deploy to Docker
This job builds a Docker image, starts a Docker container, and deploys the application.

### 6. **Web Scan Stage**

#### Fortify DAST Scan
This job performs a DAST scan using the Fortify WebInspect API.

### 7. **Results Upload**

#### Scan Results Upload
This job uploads the scan results to SSC (Software Security Center) using the Fortify CLI.

![Artifacts](images/Artifacts.PNG)

## SRE Overview

### APM (Application Performance Monitoring)

#### Prometheus

Integrated APM using Prometheus for metrics collecting.

![Prometheus](images/Prometheus.PNG)

#### Grafana

Added Prometheus as a data source on Grafana for data visualization and alerting.

![APM - springbootwebapp-1](images/APM - springbootwebapp-1.PNG)

![APM - springbootwebapp-2](images/APM - springbootwebapp-2.PNG)

### Load Balancing

#### Nginx

Used Nginx as HTTP load balancer. 

### Container Management

#### Rundeck

Integrated Rundeck with Docker to automate tasks such as starting, stopping and scaling containers.

`Grafana and Rundeck Login Credentials`

username: admin

password: admin
