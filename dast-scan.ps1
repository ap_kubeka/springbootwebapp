﻿$scanId = "3d0cc370-f93e-4033-abd4-4d2acfe03208"
$baseURL = "http://desktop-6ae6e0j:8083"

# Define the API endpoint URL for initiating the scan
$scanApiUrl = "$baseURL/webinspect/api/v1/scanner/scans/{scanId}?action=continue"

# Replace {ScanId} placeholder in the initiate scan API URL with the actual scan ID
$scanApiUrl = $scanApiUrl -replace "{ScanId}", $scanId

# Set the API endpoint URL for checking the scan status
$getStatusApiUrl = "$baseURL/webinspect/api/v1/scanner/scans/{scanId}?action=getCurrentStatus"

# Replace {ScanId} placeholder in the status API URL with the actual scan ID
$getStatusApiUrl = $getStatusApiUrl -replace "{ScanId}", $scanId

# Define the API endpoint URL for downloading the scan results
$downloadFPRApiUrl = "$baseURL/webinspect/api/v1/scanner/scans/{scanId}.fpr"

# Replace {ScanId} placeholder in the download FPR API URL with the actual scan ID
$downloadFPRApiUrl = $downloadFPRApiUrl -replace "{ScanId}", $scanId

# Set the required headers for the scan and get status API calls
$headers = @{
    "ContentType" = "application/json"
}

# Initiate the scan
$scanResponse = Invoke-RestMethod -Uri $scanApiUrl -Method Post -Headers $headers

# Parse the response JSON to check the scan status
$status = $scanResponse.scanStatus

# Print status
Write-Output "Scan Status: $status"

# Poll the API until the scan status is "Complete"
$scanStatus = ""
while ($scanStatus -ne "Complete") {
    # Send a GET request to check the scan status
    $statusResponse = Invoke-RestMethod -Uri $getStatusApiUrl -Method Get -Headers $headers
    $scanStatus = $statusResponse.scanStatus

    # Print status
    Write-Output "Scan Status: $scanStatus"
    
    # Wait for a few 5 minutes before polling again
    Start-Sleep -Seconds 300
}

# The scan is completed
Write-Output "Scan Status: $scanStatus"

# Set the path where you want to save the downloaded file
$downloadFilePath = "C:\GitLab-Runner\builds\9YkiRQhKq\0\ap_kubeka\springbootwebapp\$scanId.fpr"

# Set the required heades for the export FRP API call
$exportHeaders = @{
    "ContentType" = "application/octet-stream"
}

# Download the scan results file
Invoke-WebRequest -Uri $downloadFPRApiUrl -Headers $headers -OutFile $downloadFilePath

# The scan results file is downloaded to the specified path
Write-Output "Scan results downloaded to: $downloadFilePath"


